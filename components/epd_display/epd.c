/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    epd.c
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/

#include "epd.h"
#include "epd_port.h"
#include "logger.h"

#include <stdlib.h>
/*
 * *****************************************************************************
 * *                                                                           *
 * *                          MACROS DEFINITIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */
#define EPD_AUTOMATIC_OLD_BUFFER_USED                                   (1LU)

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           TYPE DEFINITIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */
#define EPD_INSTANCES_CNT                                               (1LU)

#define EPD_CMD_PANEL_SETTING                                           (0x00)
#define EPD_CMD_POWER_SETTING                                           (0x01)
#define EPD_CMD_POWER_OFF                                               (0x02)
#define EPD_CMD_POWER_OFF_SEQUENCE_SETTING                              (0x03)
#define EPD_CMD_POWER_ON                                                (0x04)
#define EPD_CMD_POWER_ON_MEASURE                                        (0x05)
#define EPD_CMD_BOOSTER_SOFT_START                                      (0x06)
#define EPD_CMD_DEEP_SLEEP                                              (0x07)
#define EPD_CMD_DATA_START_TRANSMISSION_1                               (0x10)
#define EPD_CMD_DATA_STOP                                               (0x11)
#define EPD_CMD_DISPLAY_REFRESH                                         (0x12)
#define EPD_CMD_DATA_START_TRANSMISSION_2                               (0x13)
#define EPD_CMD_DUAL_SPI                                                (0x15)
#define EPD_CMD_VCOM_LUT                                                (0x20)
#define EPD_CMD_W2W_LUT                                                 (0x21)
#define EPD_CMD_B2W_LUT                                                 (0x22)
#define EPD_CMD_W2B_LUT                                                 (0x23)
#define EPD_CMD_B2B_LUT                                                 (0x24)
#define EPD_CMD_PLL_CONTROL                                             (0x30)
#define EPD_CMD_TEMPERATURE_SENSOR_CALIBRATION                          (0x40)
#define EPD_CMD_TEMPERATURE_SENSOR_SELECTION                            (0x41)
#define EPD_CMD_TEMPERATURE_SENSOR_WRITE                                (0x42)
#define EPD_CMD_TEMPERATURE_SENSOR_READ                                 (0x43)
#define EPD_CMD_VCOM_AND_DATA_INTERVAL_SETTING                          (0x50)
#define EPD_CMD_LOW_POWER_DETECTION                                     (0x51)
#define EPD_CMD_TCON_SETTING                                            (0x60)
#define EPD_CMD_RESOLUTION_SETTING                                      (0x61)
#define EPD_CMD_GET_STATUS                                              (0x71)
#define EPD_CMD_AUTO_MEASURE_VCOM                                       (0x80)
#define EPD_CMD_READ_VCOM_VALUE                                         (0x81)
#define EPD_CMD_VCM_DC_SETTING                                          (0x82)
#define EPD_CMD_PARTIAL_WINDOW                                          (0x90)
#define EPD_CMD_PARTIAL_IN                                              (0x91)
#define EPD_CMD_PARTIAL_OUT                                             (0x92)
#define EPD_CMD_PROGRAM_MODE                                            (0xA0)
#define EPD_CMD_ACTIVE_PROGRAM                                          (0xA1)
#define EPD_CMD_READ_OTP_DATA                                           (0xA2)
#define EPD_CMD_POWER_SAVING                                            (0xE3)

/*
 * *****************************************************************************
 * *                                                                           *
 * *                     STATIC FUNCTION DECLARATIONS                          *
 * *                                                                           *
 * *****************************************************************************
 */

static epd_retval_e s_epd_power_on(int32_t minor);

static epd_retval_e s_epd_power_off(int32_t minor);

static epd_retval_e s_epd_config_power_setting(int32_t minor);

static epd_retval_e s_epd_config_pannel_setting(int32_t minor);

static epd_retval_e s_epd_config_dual_spi(int32_t minor);

static epd_retval_e s_epd_config_boost_softstart(int32_t minor);

static epd_retval_e s_epd_config_pll_control(int32_t minor);

static epd_retval_e s_epd_config_temperature_calib(int32_t minor);

static epd_retval_e s_epd_config_vcom_and_data_interval_setting(int32_t minor);

static epd_retval_e s_epd_config_tcon_setting(int32_t minor);

static epd_retval_e s_epd_config_tcon_resolution(int32_t minor);

static epd_retval_e s_epd_config_vcm_dc_setting(int32_t minor);

static epd_retval_e s_epd_config_flash_mode(int32_t minor);

static epd_retval_e s_epd_wait_until_busy(epd_instance_t* instance);

static epd_retval_e s_epd_update_lut(int32_t minor, epd_lut_t* lut);

/**
 *
 * @param minor
 * @param in_image
 * @param in_image_size
 * @param out_image
 * @param out_image_size
 * @return
 *
 *
 * @note 4 grayscale demo function
 *            Color display description
 *            white  gray1  gray2  black
 *      0x10|  01     01     00     00
 *      0x13|  01     00     01     00
 */
static epd_retval_e s_epd_convert_image_2bpp(
    int32_t  minor,
    epd_convertion_type_e type,
    uint8_t* in_image,
    uint32_t in_image_size,
    uint8_t* out_image,
    uint32_t out_image_size
);
/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL VARIABLES                                *
 * *                                                                           *
 * *****************************************************************************
 */
static const epd_setup_t S_EPD_CONFIG = {
    .width      =   800L,
    .height     =   480L,
    .bpp        =   E_EPD_BPP_1
};

static const unsigned char s_lut_vcom[] =
{
    0x00    ,0x0A   ,0x00   ,0x00   ,0x00   ,0x01,
    0x60    ,0x14   ,0x14   ,0x00   ,0x00   ,0x01,
    0x00    ,0x14   ,0x00   ,0x00   ,0x00   ,0x01,
    0x00    ,0x13   ,0x0A   ,0x01   ,0x00   ,0x01,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00

};
//R21
static const unsigned char s_lut_ww[] ={
    0x40    ,0x0A   ,0x00   ,0x00   ,0x00   ,0x01,
    0x90    ,0x14   ,0x14   ,0x00   ,0x00   ,0x01,
    0x10    ,0x14   ,0x0A   ,0x00   ,0x00   ,0x01,
    0xA0    ,0x13   ,0x01   ,0x00   ,0x00   ,0x01,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
};
//R22H  r
static const unsigned char s_lut_bw[] ={
    0x40    ,0x0A   ,0x00   ,0x00   ,0x00   ,0x01,
    0x90    ,0x14   ,0x14   ,0x00   ,0x00   ,0x01,
    0x00    ,0x14   ,0x0A   ,0x00   ,0x00   ,0x01,
    0x99    ,0x0C   ,0x01   ,0x03   ,0x04   ,0x01,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
};
//R23H  w
static const unsigned char s_lut_wb[] ={
    0x40    ,0x0A   ,0x00   ,0x00   ,0x00   ,0x01,
    0x90    ,0x14   ,0x14   ,0x00   ,0x00   ,0x01,
    0x00    ,0x14   ,0x0A   ,0x00   ,0x00   ,0x01,
    0x99    ,0x0B   ,0x04   ,0x04   ,0x01   ,0x01,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
};
//R24H  b
static const unsigned char s_lut_bb[] ={
    0x80    ,0x0A   ,0x00   ,0x00   ,0x00   ,0x01,
    0x90    ,0x14   ,0x14   ,0x00   ,0x00   ,0x01,
    0x20    ,0x14   ,0x0A   ,0x00   ,0x00   ,0x01,
    0x50    ,0x13   ,0x01   ,0x00   ,0x00   ,0x01,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
    0x00    ,0x00   ,0x00   ,0x00   ,0x00   ,0x00,
};

static epd_instance_t s_epd_display_instance[EPD_INSTANCES_CNT] = {
    {
         .init_config_ptr       = &S_EPD_CONFIG,
         .status                = E_EPD_STATE_NOT_INITIALIZED,
         .hw_interface_config   = {
             .reset_pin= {
                 .port = 0LU,
                 .pin = 15LU
             },
             .cmd_pin= {
                 .port = 0LU,
                 .pin = 4LU
             },
             .busy_pin= {
                 .port = 0LU,
                 .pin = 22LU
             },
             .spi_mosi= {
                 .port = 0LU,
                 .pin = 23LU
             },
             .spi_miso= {
                 .port = 0LU,
                 .pin = 19LU
             },
             .spi_clk= {
                 .port = 0LU,
                 .pin = 18LU
             },
             .spi_cs = {
                 .port = 0LU,
                 .pin = 5LU
             },
             .spi_minor     = 2LU,
             .current_speed = 1000000LU
         },
         .waveform              = E_EPD_WAVEFORM_FAST,
         .lut_set = {
             .lut_vcom = {
                 .cmd      = EPD_CMD_VCOM_LUT,
                 .lut_size = sizeof(s_lut_vcom),
                 .lut_data = s_lut_vcom
             },
             .lut_ww = {
                 .cmd      = EPD_CMD_W2W_LUT,
                 .lut_size = sizeof(s_lut_ww),
                 .lut_data = s_lut_ww
             },
             .lut_wb = {
                 .cmd      = EPD_CMD_W2B_LUT,
                 .lut_size = sizeof(s_lut_wb),
                 .lut_data = s_lut_wb
             },
             .lut_bw = {
                 .cmd      = EPD_CMD_B2W_LUT,
                 .lut_size = sizeof(s_lut_bw),
                 .lut_data = s_lut_bw
             },
             .lut_bb = {
                 .cmd      = EPD_CMD_B2B_LUT,
                 .lut_size = sizeof(s_lut_bb),
                 .lut_data = s_lut_bb
             }
         }
    }
};


/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL FUNCTIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

epd_retval_e epd_display_initialize(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;

    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

#if EPD_AUTOMATIC_OLD_BUFFER_USED == 1LU
    instance->old_image_buf.image_buf_size =
        (instance->init_config_ptr->width /
            instance->init_config_ptr->bpp) *
                instance->init_config_ptr->height;

    instance->old_image_buf.image_buf =
        (uint8_t*)malloc(
            (instance->init_config_ptr->width /
                instance->init_config_ptr->bpp) *
                    instance->init_config_ptr->height);

    if (NULL == instance->old_image_buf.image_buf) {
        LOG_ERROR("NO MEMORY FOR OLD IMAGE BUFFER");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }
#endif

    instance->new_image_buf.image_buf_size =
        (instance->init_config_ptr->width /
            instance->init_config_ptr->bpp) *
                instance->init_config_ptr->height;

    instance->new_image_buf.image_buf =
        (uint8_t*)malloc(
            (instance->init_config_ptr->width /
                instance->init_config_ptr->bpp) *
                    instance->init_config_ptr->height);

    if (NULL == instance->new_image_buf.image_buf) {
        LOG_ERROR("NO MEMORY FOR NEW IMAGE BUFFER");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = epd_hw_busy_pin_config(&instance->hw_interface_config);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO CONFIGURE EPD BUSY PIN");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = epd_hw_cmd_data_pin_config(&instance->hw_interface_config);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO CONFIGURE EPD COMMAND/DATA PIN");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = epd_hw_reset_pin_config(&instance->hw_interface_config);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET CONFIGURE EPD RESET PIN");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = epd_hw_spi_config(&instance->hw_interface_config);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET CONFIGURE SPI");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    epd_hw_reset(&instance->hw_interface_config);

    retval = s_epd_config_power_setting(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET POWER SETTINGS");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_power_on(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO POWER EPD ON");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_config_pannel_setting(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET PANNEL SETTINGS");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_config_pll_control(minor);
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO SET PLL SETTINGS");
            retval = E_EPD_INTERNAL_ERROR;
            goto exit;
        }

    retval = s_epd_config_dual_spi(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO DEINITIALIZE DUAL SPI");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_config_tcon_setting(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET TCON SETTINGS");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_config_tcon_resolution(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET TCON RESOLUTION SETTINGS");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_config_vcom_and_data_interval_setting(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET VCOM AND DATA INTERVAL SETTINGS");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_config_vcm_dc_setting(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SET VCM DC");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_update_lut(
                 minor,
                 &s_epd_display_instance[minor].lut_set.lut_vcom);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO UPDATE VCOM LUT");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_update_lut(
                 minor,
                 &s_epd_display_instance[minor].lut_set.lut_ww);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO PUPDATE WW LUT");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_update_lut(
                 minor,
                 &s_epd_display_instance[minor].lut_set.lut_wb);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO UPDATE WB LUT");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_update_lut(
                 minor,
                 &s_epd_display_instance[minor].lut_set.lut_bw);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO UPDATE BW LUT");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    retval = s_epd_update_lut(
                 minor,
                 &s_epd_display_instance[minor].lut_set.lut_bb);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO UPDATE BB LUT");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

#if 0
    retval = epd_display_clear(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO CLEAR EPD DISPLAY");
        goto exit;
    }
#endif

    retval = s_epd_power_off(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO POWER DISPLAY OFF");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

exit:
    return retval;
}

epd_retval_e epd_change_waveform_type(
    int32_t minor,
    epd_display_waveform_type_e waveform
)
{
    epd_retval_e retval = E_EPD_OK;

    return retval;
}

epd_retval_e epd_display_write_image(
    int32_t minor,
    uint8_t* data,
    uint32_t data_size
)
{
    epd_retval_e retval = E_EPD_OK;
    uint8_t cmd = 0U;

    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    if (NULL == data) {
        LOG_ERROR("NULL DATA POINTER PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }
    epd_instance_t* instance = &s_epd_display_instance[minor];

#if EPD_AUTOMATIC_OLD_BUFFER_USED == 1LU
    if (instance->old_image_buf.image_buf == NULL) {
        LOG_ERROR("NULL OLD IMAGE BUFFER PTR");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    if (instance->old_image_buf.image_buf_size < (data_size / 2LU)) {
        LOG_ERROR("OLD BUFFER SIZE SMALLER THAN REQUESTED IMAGE");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }
#endif

    if (instance->new_image_buf.image_buf == NULL) {
        LOG_ERROR("NULL NEW IMAGE BUFFER PTR");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

    if (instance->new_image_buf.image_buf_size < (data_size / 2LU)) {
        LOG_ERROR("NEW BUFFER SIZE SMALLER THAN REQUESTED IMAGE");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

#if EPD_AUTOMATIC_OLD_BUFFER_USED == 1LU
    cmd = EPD_CMD_DATA_START_TRANSMISSION_1;

    retval = s_epd_convert_image_2bpp(
                minor,
                E_EPD_CONVERTION_TO_OLD,
                data,
                data_size,
                instance->old_image_buf.image_buf,
                instance->old_image_buf.image_buf_size);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO GENERATE OLD BUFFER");
        goto exit;
    }

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                &instance->hw_interface_config,
                instance->old_image_buf.image_buf,
                instance->old_image_buf.image_buf_size
            );
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
        goto exit;
    }
#endif

    retval = s_epd_convert_image_2bpp(
                minor,
                E_EPD_CONVERTION_TO_NEW,
                data,
                data_size,
                instance->new_image_buf.image_buf,
                instance->new_image_buf.image_buf_size
            );
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO GENERATE NEW BUFFER");
        goto exit;
    }

    retval = epd_hw_wait_for_tx_end(&instance->hw_interface_config);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO WAIT UNTIL IMAGE IS TRANSMITTED");
        goto exit;
    }

    cmd = EPD_CMD_DATA_START_TRANSMISSION_2;
    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
        goto exit;
    }
    retval = epd_hw_write_data(
                &instance->hw_interface_config,
                instance->new_image_buf.image_buf,
                instance->new_image_buf.image_buf_size
            );
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
        goto exit;
    }
    retval = epd_hw_wait_for_tx_end(&instance->hw_interface_config);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO WAIT UNTIL IMAGE IS TRANSMITTED");
        goto exit;
    }

exit:
    return retval;
}

epd_retval_e epd_display_refresh(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    retval = s_epd_power_on(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO POWER ON THE DISPLAY");
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_DISPLAY_REFRESH;

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY REFRESH COMMAND");
        goto exit;
    }

    /* HW error - this delay is necessary here*/
    epd_hw_delay(100LU);

    retval = s_epd_wait_until_busy(instance);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILURE DURING WAITING FOR BUSY PIN TO DEASSERT");
        goto exit;
    }

    retval = s_epd_power_off(minor);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO POWER OFF THE DISPLAY");
        goto exit;
    }

exit:
    return retval;
}

epd_retval_e epd_display_clear(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    LOG_INFO("CLEARING EPD DISPLAY");
    for (uint32_t cnt = 1LU; cnt != 0LU; --cnt) {

#if EPD_AUTOMATIC_OLD_BUFFER_USED == 1LU
        memset(
            instance->old_image_buf.image_buf,
            0x00LU,
            instance->old_image_buf.image_buf_size);

        retval = epd_hw_write_command(
            &instance->hw_interface_config,
            EPD_CMD_DATA_START_TRANSMISSION_1);
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
            goto exit;
        }

        LOG_INFO("SENDING OLD IMAGE WITH SIZE: %u",
                instance->old_image_buf.image_buf_size);
        retval = epd_hw_write_data(
                    &instance->hw_interface_config,
                    instance->old_image_buf.image_buf,
                    instance->old_image_buf.image_buf_size
                );
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
            goto exit;
        }
#endif

        memset(
            instance->new_image_buf.image_buf,
            0x00LU,
            instance->new_image_buf.image_buf_size);

        retval = epd_hw_write_command(
                     &instance->hw_interface_config,
                     EPD_CMD_DATA_START_TRANSMISSION_1);
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
            goto exit;
        }

        retval = epd_hw_write_data(
                    &instance->hw_interface_config,
                    instance->old_image_buf.image_buf,
                    instance->old_image_buf.image_buf_size
                );
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO SETUP DISPLAY DATA TRANSMISSION COMMAND");
            goto exit;
        }

        retval = epd_hw_wait_for_tx_end(&instance->hw_interface_config);
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO WAIT UNTIL IMAGE IS TRANSMITTED");
            goto exit;
        }

        retval = epd_display_refresh(minor);
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO REFRESH THE DISPLAY");
            goto exit;
        }
    }
exit:
    return retval;
}

epd_retval_e epd_deep_sleep(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];
    uint8_t cmd = EPD_CMD_DEEP_SLEEP;
    uint8_t data[1LU] = {0xA5};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY DEEP SLEEP COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY DEEP SLEEP DATA COMMAND");
        goto exit;
    }

exit:
    return retval;
}
/*
 * *****************************************************************************
 * *                                                                           *
 * *                            STATIC FUNCTIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */

static epd_retval_e s_epd_power_on(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];
    uint8_t cmd = EPD_CMD_POWER_ON;
    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY REFRESH COMMAND");
        goto exit;
    }

    retval = s_epd_wait_until_busy(instance);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILURE DURING WAITING FOR BUSY PIN TO DEASSERT");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_power_off(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];
    uint8_t cmd = EPD_CMD_POWER_OFF;
    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SETUP DISPLAY REFRESH COMMAND");
        goto exit;
    }

    retval = s_epd_wait_until_busy(instance);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILURE DURING WAITING FOR BUSY PIN TO DEASSERT");
        goto exit;
    }

exit:
    return retval;
}


static epd_retval_e s_epd_config_power_setting(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_POWER_SETTING;
    uint8_t data[4LU] = {0x07, 0x17, 0x2F, 0x25, 0x03};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_config_pannel_setting(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_PANEL_SETTING;
    uint8_t data[1LU] = {0xBF};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_config_dual_spi(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_DUAL_SPI;
    uint8_t data[1LU] = {0x00};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_config_boost_softstart(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_BOOSTER_SOFT_START;
    uint8_t data[3LU] = {0xC7, 0xCC, 0x28};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_config_pll_control(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_PLL_CONTROL;
    uint8_t data[1LU] = {0x06};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_config_temperature_calib(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_TEMPERATURE_SENSOR_SELECTION;
    uint8_t data[1LU] = {0x00};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }
exit:
    return retval;
}

static epd_retval_e s_epd_config_vcom_and_data_interval_setting(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_VCOM_AND_DATA_INTERVAL_SETTING;

#if EPD_AUTOMATIC_OLD_BUFFER_USED == 1LU
    uint8_t data[2LU] = {0x11, 0x07};
#else
    uint8_t data[2LU] = {0x19, 0x07};
#endif

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }
exit:
    return retval;
}

static epd_retval_e s_epd_config_tcon_setting(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_TCON_SETTING;
    uint8_t data[1LU] = {0x22};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }
exit:
    return retval;
}

static epd_retval_e s_epd_config_tcon_resolution(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_RESOLUTION_SETTING;
    uint8_t data[4LU] = {
        (uint8_t)(instance->init_config_ptr->width  >> 8),
        (uint8_t)(instance->init_config_ptr->width  >> 0),
        (uint8_t)(instance->init_config_ptr->height >> 8),
        (uint8_t)(instance->init_config_ptr->height >> 0)
    };

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_config_vcm_dc_setting(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    uint8_t cmd = EPD_CMD_VCM_DC_SETTING;
    uint8_t data[1LU] = {0x1E};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_config_flash_mode(int32_t minor)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];
    uint8_t cmd = 0xE5;
    uint8_t data[1LU] = {0x03};

    retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND COMMAND");
        goto exit;
    }

    retval = epd_hw_write_data(
                 &instance->hw_interface_config,
                 data,
                 sizeof(data));
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND DATA");
        goto exit;
    }

exit:
    return retval;
}

static epd_retval_e s_epd_wait_until_busy(epd_instance_t* instance)
{
    epd_retval_e retval = E_EPD_OK;
    if (NULL == instance) {
        LOG_ERROR("INVALID INSTANCE PROVIDED EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    uint8_t cmd = EPD_CMD_GET_STATUS;

    uint32_t max_delay_ms = 300LU;
    uint32_t busy_state = 0LU;
    uint32_t check_delay_ms = 10LU;

    do {
        retval = epd_hw_write_command(&instance->hw_interface_config, cmd);
        if (E_EPD_OK != retval) {
            LOG_ERROR("FAILED TO WRITE 'EPD_CMD_GET_STATUS' COMMAND");
            break;
        }

        busy_state = epd_hw_read_busy_pin_state(&instance->hw_interface_config);
        if (-1L == busy_state) {
            LOG_ERROR("FAILED TO READ BUSY PIN STATE");
            retval = E_EPD_INTERNAL_ERROR;
            break;
        }

        if (0LU == busy_state) {
            epd_hw_delay(check_delay_ms);
            max_delay_ms -= check_delay_ms;
        }
    } while ((0LU == busy_state) && (0LU != max_delay_ms));

exit:
    return retval;
}

static epd_retval_e s_epd_update_lut(int32_t minor, epd_lut_t* lut)
{
    epd_retval_e retval = E_EPD_OK;
    if (minor >= EPD_INSTANCES_CNT) {
        LOG_ERROR("MINOR EXCEEDS AVAILABLE EPD INSTANCES COUNT");
        retval = E_EPD_INVALID_INSTANCE;
        goto exit;
    }

    if (NULL == lut) {
        LOG_ERROR("NULL LUT STRUCTURE PTR PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    epd_instance_t* instance = &s_epd_display_instance[minor];

    retval = epd_hw_write_command(&instance->hw_interface_config, lut->cmd);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND LUT COMMAND: %hhu", lut->cmd);
        goto exit;
    }

    retval = epd_hw_write_data(
                &instance->hw_interface_config,
                (uint8_t*)lut->lut_data,
                lut->lut_size
            );
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO SEND LUT DATA");
        goto exit;
    }

exit:
    return retval;
}

/**
 *
 * @param minor
 * @param in_image
 * @param in_image_size
 * @param out_image
 * @param out_image_size
 * @return
 *
 *
 * @note 4 grayscale demo function
 *            Color display description
 *            white  gray1  gray2  black
 *      0x10|  01     01     00     00
 *      0x13|  01     00     01     00
 */
static epd_retval_e s_epd_convert_image_2bpp(
    int32_t  minor,
    epd_convertion_type_e type,
    uint8_t* in_image,
    uint32_t in_image_size,
    uint8_t* out_image,
    uint32_t out_image_size
)
{
    epd_retval_e retval = E_EPD_OK;

    if (E_EPD_CONVERTION_TO_OLD == type) {
        for (uint32_t i = 0LU; i < in_image_size; i += 2LU) {
            uint8_t out_pixel = 0LU;

            uint8_t lower_four_pixels = in_image[i];
            uint8_t higher_four_pixels = in_image[i + 1LU];

#if 1
            /* First pixel to the MSBit position */
            out_pixel |= (((lower_four_pixels  & 0x02LU) >> 1LU) << 4LU) |
                         (((lower_four_pixels  & 0x08LU) >> 3LU) << 5LU) |
                         (((lower_four_pixels  & 0x20LU) >> 5LU) << 6LU) |
                         (((lower_four_pixels  & 0x80LU) >> 7LU) << 7LU) |
                         (((higher_four_pixels & 0x02LU) >> 1LU) << 0LU) |
                         (((higher_four_pixels & 0x08LU) >> 3LU) << 1LU) |
                         (((higher_four_pixels & 0x20LU) >> 5LU) << 2LU) |
                         (((higher_four_pixels & 0x80LU) >> 7LU) << 3LU);
#else
            /* First pixel to the MSBit position */
            out_pixel  = ((lower_four_pixels  & 0x02LU) << 6LU) |
                         ((lower_four_pixels  & 0x08LU) << 3LU) |
                         ((lower_four_pixels  & 0x20LU) << 0LU) |
                         ((lower_four_pixels  & 0x80LU) >> 3LU) |
                         ((higher_four_pixels & 0x02LU) << 2LU) |
                         ((higher_four_pixels & 0x08LU) >> 1LU) |
                         ((higher_four_pixels & 0x20LU) >> 4LU) |
                         ((higher_four_pixels & 0x80LU) >> 7LU);
#endif
            out_image[i / 2LU] = out_pixel;
        }
    }
    else if (E_EPD_CONVERTION_TO_NEW == type) {
        for (uint32_t i = 0LU; i < in_image_size; i += 2LU) {
            uint8_t out_pixel = 0LU;

            uint8_t lower_four_pixels = in_image[i];
            uint8_t higher_four_pixels = in_image[i + 1LU];
#if 1
            /* First pixel to the MSBit position */
            out_pixel |= (((lower_four_pixels  & 0x01LU) >> 0LU) << 4LU) |
                         (((lower_four_pixels  & 0x04LU) >> 2LU) << 5LU) |
                         (((lower_four_pixels  & 0x10LU) >> 4LU) << 6LU) |
                         (((lower_four_pixels  & 0x40LU) >> 6LU) << 7LU) |
                         (((higher_four_pixels & 0x01LU) >> 0LU) << 0LU) |
                         (((higher_four_pixels & 0x04LU) >> 2LU) << 1LU) |
                         (((higher_four_pixels & 0x10LU) >> 4LU) << 2LU) |
                         (((higher_four_pixels & 0x40LU) >> 6LU) << 3LU);
#else
             /* First pixel to the MSBit position */
            out_pixel |= ((lower_four_pixels  & 0x01LU) << 7LU) |
                         ((lower_four_pixels  & 0x04LU) << 4LU) |
                         ((lower_four_pixels  & 0x10LU) << 1LU) |
                         ((lower_four_pixels  & 0x40LU) >> 2LU) |
                         ((higher_four_pixels & 0x01LU) << 3LU) |
                         ((higher_four_pixels & 0x04LU) >> 0LU) |
                         ((higher_four_pixels & 0x10LU) >> 3LU) |
                         ((higher_four_pixels & 0x40LU) >> 6LU);
#endif
            out_image[i / 2LU] = out_pixel;
        }
    }

    return retval;
}
