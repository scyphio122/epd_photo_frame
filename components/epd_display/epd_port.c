/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    epd_port.c
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/

#include "epd_port.h"
#include "epd_types.h"
#include <stdint-gcc.h>

#include "logger.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "driver/spi_master.h"
#include "driver/spi_common.h"
#include "driver/gpio.h"

/*
 * *****************************************************************************
 * *                                                                           *
 * *                          MACROS DEFINITIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */


/*
 * *****************************************************************************
 * *                                                                           *
 * *                           TYPE DEFINITIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

/*
 * *****************************************************************************
 * *                                                                           *
 * *                     STATIC FUNCTION DECLARATIONS                          *
 * *                                                                           *
 * *****************************************************************************
 */
static epd_retval_e s_epd_hw_assert_command(epd_hw_interface_t* hw_interface);

static epd_retval_e s_epd_hw_assert_data(epd_hw_interface_t* hw_interface);
/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL VARIABLES                                *
 * *                                                                           *
 * *****************************************************************************
 */
static spi_device_handle_t s_epd_spi_handle;
static spi_bus_config_t buscfg;

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL FUNCTIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

epd_retval_e epd_hw_busy_pin_config(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = (1LU << hw_interface->busy_pin.pin);
    //disable pull-down mode
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    //disable pull-up mode
    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

    exit:
        return retval;
}

epd_retval_e epd_hw_cmd_data_pin_config(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as input mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = (1LU << hw_interface->cmd_pin.pin);
    //disable pull-down mode
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    //disable pull-up mode
    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

exit:
    return retval;
}

epd_retval_e epd_hw_reset_pin_config(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as input mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = (1LU << hw_interface->reset_pin.pin);
    //disable pull-down mode
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    //disable pull-up mode
    io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
    //configure GPIO with the given settings
    gpio_config(&io_conf);

exit:
    return retval;
}

epd_retval_e epd_hw_spi_config(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    esp_err_t ret;

    buscfg.miso_io_num = hw_interface->spi_miso.pin;
    buscfg.mosi_io_num = hw_interface->spi_mosi.pin;
    buscfg.sclk_io_num = hw_interface->spi_clk.pin;
    buscfg.quadwp_io_num = -1;
    buscfg.quadhd_io_num = -1;
    buscfg.max_transfer_sz = -1;
    buscfg.flags = SPICOMMON_BUSFLAG_MASTER;

    /* Initialize the SPI bus with DMA channel 1 (last argument) */
    ret = spi_bus_initialize(hw_interface->spi_minor, &buscfg, 1LU);
    if (ESP_OK != ret) {
        LOG_ERROR("FAILED TO INITIALIZE SPI BUS");
        retval = E_EPD_IO_ERROR;
        goto exit;
    }

    spi_device_interface_config_t devcfg = {
        .clock_speed_hz = hw_interface->current_speed,
        .mode = 0LU,
        .command_bits = 0LU,
        .address_bits = 0LU,
        .dummy_bits = 0LU,
        .spics_io_num = hw_interface->spi_cs.pin,
        .queue_size = 2LU,
        .duty_cycle_pos = 128LU,
        .pre_cb = NULL,
        .post_cb = NULL,
        .cs_ena_posttrans = 0,
        .cs_ena_pretrans = 0,
        .flags = SPI_DEVICE_HALFDUPLEX
    };

    /* Register SPI 'device' in the driver */
    ret = spi_bus_add_device(
              hw_interface->spi_minor,
              &devcfg,
              &s_epd_spi_handle
          );
    if (ESP_OK != ret) {
        LOG_ERROR("FAILED TO ADD SPI DEVICE");
        retval = E_EPD_IO_ERROR;
        goto exit;
    }

exit:
    return retval;
}

void epd_hw_delay(uint32_t ms)
{
    vTaskDelay(pdMS_TO_TICKS(ms));
}

epd_retval_e epd_hw_reset(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    gpio_set_level(hw_interface->reset_pin.pin, 1LU);
    epd_hw_delay(200LU);
    gpio_set_level(hw_interface->reset_pin.pin, 0LU);
    epd_hw_delay(200LU);
    gpio_set_level(hw_interface->reset_pin.pin, 1LU);
    epd_hw_delay(200LU);

exit:
    return retval;
}

int32_t epd_hw_read_busy_pin_state(epd_hw_interface_t* hw_interface)
{
    int32_t retval = 0LU;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = -1;
        goto exit;
    }

    retval = (int32_t)gpio_get_level(hw_interface->busy_pin.pin);
exit:
    return retval;
}

epd_retval_e epd_hw_write_command(
    epd_hw_interface_t* hw_interface,
    uint8_t command
)
{
    epd_retval_e retval = E_EPD_OK;
    esp_err_t ret;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    retval = s_epd_hw_assert_command(hw_interface);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO ASSERT CMD STATE");
        goto exit;
    }

    spi_transaction_t transaction;
    memset(&transaction, 0LU, sizeof(transaction));
    transaction.flags = SPI_TRANS_USE_TXDATA;
    transaction.tx_data[0] = command;
    transaction.length = 8LU;

    ret = spi_device_transmit(
                        s_epd_spi_handle,
                        &transaction
                    );
#if 0
    esp_err_t ret = spi_device_queue_trans(
                        s_epd_spi_handle,
                        &transaction,
                        pdMS_TO_TICKS(100)
                    );
#endif
    if (ESP_OK != ret) {
        LOG_ERROR("FAILED TO ENQUEUE SPI TRANSACTION. CODE: %s", esp_err_to_name(ret));
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }

exit:
    return retval;
}

epd_retval_e epd_hw_write_data(
    epd_hw_interface_t* hw_interface,
    uint8_t* data,
    uint32_t data_size
)
{
    epd_retval_e retval = E_EPD_OK;
    esp_err_t ret;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    if (NULL == data) {
        LOG_ERROR("NULL DATA POINTER PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    if (0LU == data_size) {
        LOG_ERROR("ZERO LENGTH DATA PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    retval = s_epd_hw_assert_data(hw_interface);
    if (E_EPD_OK != retval) {
        LOG_ERROR("FAILED TO ASSERT DATA STATE");
        goto exit;
    }

    spi_transaction_t transaction;

    uint32_t transaction_size = 0LU;
    uint32_t current_offset = 0LU;

    while (0LU != data_size) {
        memset(&transaction, 0LU, sizeof(transaction));

        if (data_size > 2048LU) {
            transaction_size = 2048LU;
        }
        else {
            transaction_size = data_size;
        }

        transaction.tx_buffer = &data[current_offset];
        transaction.length = transaction_size * 8LU;

        ret = spi_device_transmit(
                            s_epd_spi_handle,
                            &transaction
                        );
#if 0
        esp_err_t ret = spi_device_queue_trans(
                            s_epd_spi_handle,
                            &transaction,
                            pdMS_TO_TICKS(100)
                        );
#endif
        if (ESP_OK != ret) {
            LOG_ERROR("FAILED TO ENQUEUE SPI TRANSACTION");
            retval = E_EPD_INTERNAL_ERROR;
            goto exit;
        }

        current_offset += transaction_size;
        data_size -= transaction_size;
    }

exit:
    return retval;
}

epd_retval_e epd_hw_wait_for_tx_end(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

#if 0
    spi_transaction_t* spi_transaction = NULL;
    esp_err_t ret = spi_device_get_trans_result(
                        s_epd_spi_handle,
                        &spi_transaction,
                        pdMS_TO_TICKS(1000LU)
                    );
    if (ESP_ERR_TIMEOUT == ret) {
        LOG_ERROR("SPI TRANSACTION TIMEOUT");
        retval = E_EPD_TIMEOUT;
        goto exit;
    }
    else if (ESP_ERR_INVALID_ARG == ret) {
        LOG_ERROR("INVALID ARGUMENT PROVIDED");
        retval = E_EPD_INTERNAL_ERROR;
        goto exit;
    }
#endif
exit:
    return retval;
}
/*
 * *****************************************************************************
 * *                                                                           *
 * *                            STATIC FUNCTIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */

static epd_retval_e s_epd_hw_assert_command(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    gpio_set_level(hw_interface->cmd_pin.pin, 0LU);

exit:
    return retval;
}

static epd_retval_e s_epd_hw_assert_data(epd_hw_interface_t* hw_interface)
{
    epd_retval_e retval = E_EPD_OK;

    if (NULL == hw_interface) {
        LOG_ERROR("NUL EPD HW INTERFACE STRUCT PROVIDED");
        retval = E_EPD_INVALID_ARGUMENT;
        goto exit;
    }

    gpio_set_level(hw_interface->cmd_pin.pin, 1LU);

exit:
    return retval;
}
