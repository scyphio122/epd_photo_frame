/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    epd_port.h
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/
#ifndef BSP_EPD_DISPLAY_EPD_PORT_H_
#define BSP_EPD_DISPLAY_EPD_PORT_H

#include "epd_types.h"

epd_retval_e epd_hw_busy_pin_config(epd_hw_interface_t* hw_interface);

epd_retval_e epd_hw_cmd_data_pin_config(epd_hw_interface_t* hw_interface);

epd_retval_e epd_hw_reset_pin_config(epd_hw_interface_t* hw_interface);

epd_retval_e epd_hw_spi_config(epd_hw_interface_t* hw_interface);

void epd_hw_delay(uint32_t ms);

epd_retval_e epd_hw_reset(epd_hw_interface_t* hw_interface);

epd_retval_e epd_hw_write_command(
    epd_hw_interface_t* hw_interface,
    uint8_t command
);

epd_retval_e epd_hw_write_data(
    epd_hw_interface_t* hw_interface,
    uint8_t* data,
    uint32_t data_size
);

int32_t epd_hw_read_busy_pin_state(epd_hw_interface_t* hw_interface);

epd_retval_e epd_hw_assert_command(epd_hw_interface_t* hw_interface);

epd_retval_e epd_hw_assert_data(epd_hw_interface_t* hw_interface);

epd_retval_e epd_hw_wait_for_tx_end(epd_hw_interface_t* hw_interface);

#endif /* BSP_EPD_DISPLAY_EPD_PORT_H_ */
