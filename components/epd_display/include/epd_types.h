/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    epd_types.h
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/
#ifndef BSP_EPD_DISPLAY_EPD_TYPES_H_
#define BSP_EPD_DISPLAY_EPD_TYPES_H_

#include <stdint-gcc.h>

typedef enum
{
    E_EPD_OK,
    E_EPD_INVALID_INSTANCE,
    E_EPD_INVALID_ARGUMENT,
    E_EPD_BUSY,
    E_EPD_TIMEOUT,
    E_EPD_INTERNAL_ERROR,
    E_EPD_IO_ERROR
} epd_retval_e;

typedef enum
{
    E_EPD_BPP_1 = 8,
    E_EPD_BPP_2 = 4,
    E_EPD_BPP_4 = 2,
    E_EPD_BPP_8 = 1
} epd_bpp_e;

typedef enum
{
    E_EPD_STATE_NOT_INITIALIZED,
    E_EPD_STATE_IDLE,
    E_EPD_STATE_UPDATING_FRAMEBUFFER,
    E_EPD_STATE_REFRESHING
} epd_state_e;

typedef enum
{
    E_EPD_WAVEFORM_FAST,
    E_EPD_WAVEFORM_PRECISE
} epd_display_waveform_type_e;

typedef struct
{
    int32_t     width;
    int32_t     height;
    epd_bpp_e   bpp;
} epd_setup_t;

typedef struct
{
    int32_t port;
    int32_t pin;
} epd_hw_gpio_pin_t;

typedef struct
{
    epd_hw_gpio_pin_t reset_pin;
    epd_hw_gpio_pin_t cmd_pin;
    epd_hw_gpio_pin_t busy_pin;
    epd_hw_gpio_pin_t spi_mosi;
    epd_hw_gpio_pin_t spi_miso;
    epd_hw_gpio_pin_t spi_clk;
    epd_hw_gpio_pin_t spi_cs;
    int32_t spi_minor;
    int32_t current_speed;
} epd_hw_interface_t;

typedef struct
{
    uint8_t         cmd;
    uint32_t        lut_size;
    const uint8_t*  lut_data;
} epd_lut_t;

typedef struct
{
    epd_lut_t lut_vcom;
    epd_lut_t lut_ww;
    epd_lut_t lut_wb;
    epd_lut_t lut_bw;
    epd_lut_t lut_bb;
} epd_lut_set_t;

typedef struct
{
    uint8_t* image_buf;
    uint32_t image_buf_size;
} epd_image_buf_t;

typedef struct
{
    const epd_setup_t*          init_config_ptr;
    epd_state_e                 status;
    epd_hw_interface_t          hw_interface_config;
    epd_display_waveform_type_e waveform;
    epd_lut_set_t               lut_set;
    epd_image_buf_t             old_image_buf;
    epd_image_buf_t             new_image_buf;
} epd_instance_t;

typedef enum
{
    E_EPD_CONVERTION_TO_OLD,
    E_EPD_CONVERTION_TO_NEW
} epd_convertion_type_e;

#endif /* BSP_EPD_DISPLAY_EPD_TYPES_H_ */
