/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    log.h
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 *
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *
 * =============================================================================
 **/
#ifndef UTILS_LOGS_LOG_H_
#define UTILS_LOGS_LOG_H_

#include <string.h>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

typedef enum
{
    LOG_TYPE_TRACE,
    LOG_TYPE_DEBUG,
    LOG_TYPE_INFO,
    LOG_TYPE_WARN,
    LOG_TYPE_ERROR,
    LOG_TYPE_IMPORTANT
} log_type_e;

int log_init(void);

void log_deinit(void);

void log_write(log_type_e type, const char* filename, const int line, char* log_text, ...);

void log_flush(void);

#ifndef LOG_TAG
    #define LOG_TAG ""
#endif

#define LOG_TRACE(x...)        log_write(LOG_TYPE_TRACE,       __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_DEBUG(x...)        log_write(LOG_TYPE_DEBUG,       __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_INFO(x...)         log_write(LOG_TYPE_INFO,        __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_WARN(x...)         log_write(LOG_TYPE_WARN,        __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_ERROR(x...)        log_write(LOG_TYPE_ERROR,       __FILENAME__, __LINE__, LOG_TAG x)
#define LOG_IMPORTANT(x...)    log_write(LOG_TYPE_IMPORTANT,   __FILENAME__, __LINE__, LOG_TAG x)

#define LOG_BG_ORANGE          "\033[48;5;202m"
#define LOG_BG_VIOLET          "\033[48;5;129m"


#endif /* UTILS_LOGS_LOG_H_ */
