/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    log.c
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 *
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *
 * =============================================================================
 **/

#include "logger.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define LOG_BUFFER_SIZE                                         256LU

int log_init(void)
{
    return 0L;
}

void log_deinit(void)
{
    return;
}

void log_write(
    log_type_e type,
    const char* filename,
    const int line,
    char* log_text,
    ...
)
{
    char log_buffer[LOG_BUFFER_SIZE];

    uint32_t tick_count = xTaskGetTickCount();
    uint32_t secs    = tick_count / 1000;
    uint8_t  hours   = secs / 3600;
    uint8_t  minutes = (secs / 60) % 60;
    secs = secs % 60;

    switch (type) {
        case LOG_TYPE_TRACE: {
            snprintf(
                 log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02u.%03u \e[36;1mTRACE: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_DEBUG: {
            snprintf(
                log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02u.%03u \e[34;1mDEBUG: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_INFO: {
            snprintf(
                log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02u.%03u \e[32;1mINFO: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_WARN: {
            snprintf(
                log_buffer,
                 LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02u.%03u \e[33;1mWARNING: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_ERROR: {
            snprintf(
                 log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02u.%03u \e[31;1mERROR: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;

        case LOG_TYPE_IMPORTANT: {
            snprintf(
                log_buffer, LOG_BUFFER_SIZE - 1,
                 "%02hu:%02hu:%02u.%03u \e[35;1mIMPORTANT: %s:%d \e[0m ",
                 hours,
                 minutes,
                 secs,
                 tick_count % 1000,
                 filename,
                 line
             );
        } break;
    }

    char* header = NULL;
    uint32_t header_size = strlen(log_buffer);
    header = (char*)((uint32_t)log_buffer + header_size);

    va_list args;
    va_start(args, log_text);
    vsprintf(header, log_text, args);
    va_end(args);

    header = (char*)((uint32_t)log_buffer + strlen(log_buffer));
    *header = '\r';
    *(header + 1) = '\n';
    *(header + 2) = '\0';

    printf(log_buffer);
}

