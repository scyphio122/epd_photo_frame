/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    wifi.c
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/

#include "wifi.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "logger.h"

#include "wifi_config.h"

/*
 * *****************************************************************************
 * *                                                                           *
 * *                          MACROS DEFINITIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */

/* The event group allows multiple bits for each event, but we only care about
 *  two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT                                      BIT0
#define WIFI_FAIL_BIT                                           BIT1


/*
 * *****************************************************************************
 * *                                                                           *
 * *                           TYPE DEFINITIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

/*
 * *****************************************************************************
 * *                                                                           *
 * *                     STATIC FUNCTION DECLARATIONS                          *
 * *                                                                           *
 * *****************************************************************************
 */
static void s_wifi_event_handler(
    void* arg,
    esp_event_base_t event_base,
    int32_t event_id,
    void* event_data
);

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL VARIABLES                                *
 * *                                                                           *
 * *****************************************************************************
 */
static const char *TAG = "wifi station";

static EventGroupHandle_t s_wifi_event_group;

static int s_retry_num = 0;

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL FUNCTIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

int wifi_initialize(void)
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if ((ESP_ERR_NVS_NO_FREE_PAGES == ret) ||
        (ESP_ERR_NVS_NEW_VERSION_FOUND == ret))
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    int retval = 0L;
    s_wifi_event_group = xEventGroupCreate();
    if (0LU == s_wifi_event_group) {
        LOG_ERROR("FAILED TO CREATE WIFI EVENT GROUP");
        retval = -1L;
        goto exit;
    }

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        WIFI_EVENT,
        ESP_EVENT_ANY_ID,
        &s_wifi_event_handler,
        NULL,
        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        IP_EVENT,
        IP_EVENT_STA_GOT_IP,
        &s_wifi_event_handler,
        NULL,
        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASS,
            /* Setting a password implies station will connect to all security
             * modes including WEP/WPA. However these modes are deprecated and
             * not advisable to be used. Incase your Access point doesn't
             * support WPA2, these mode can be enabled by commenting
             *  below line */
            .threshold.authmode = WIFI_AUTH_METHOD,
            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT)
     * or connection failed for the maximum number of re-tries (WIFI_FAIL_BIT).
     * The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(
                           s_wifi_event_group,
                           WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                           pdFALSE,
                           pdFALSE,
                           portMAX_DELAY
                       );

    /* xEventGroupWaitBits() returns the bits before the call returned, hence
     * we can test which event actually happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s", WIFI_SSID);
    }
    else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s", WIFI_SSID);
    }
    else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(
        IP_EVENT,
        IP_EVENT_STA_GOT_IP,
        instance_got_ip));

    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(
        WIFI_EVENT,
        ESP_EVENT_ANY_ID,
        instance_any_id));

    vEventGroupDelete(s_wifi_event_group);

exit:
    return retval;
}

void wifi_scan_networks(void)
{

}

/*
 * *****************************************************************************
 * *                                                                           *
 * *                            STATIC FUNCTIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */

static void s_wifi_event_handler(
    void* arg,
    esp_event_base_t event_base,
    int32_t event_id,
    void* event_data
)
{
    if( (WIFI_EVENT == event_base) &&
        (WIFI_EVENT_STA_START == event_id))
    {
        esp_wifi_connect();
    }
    else if ((WIFI_EVENT == event_base) &&
        (WIFI_EVENT_STA_DISCONNECTED == event_id))
    {
        if (s_retry_num < WIFI_MAXIMUM_RETRY_CNT) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        }
        else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }

        ESP_LOGI(TAG,"connect to the AP fail");
    }
    else if ((IP_EVENT == event_base) &&
        (IP_EVENT_STA_GOT_IP == event_id))
    {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

