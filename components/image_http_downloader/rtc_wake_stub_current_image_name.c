/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    rtc_wake_stub_current_image_name.c
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/

#include "rtc_wake_stub_current_image_name.h"

#include "esp_sleep.h"
#include "esp_attr.h"
#include "esp32/rom/rtc.h"
#include "esp32/rom/ets_sys.h"

#include "logger.h"

/*
 * *****************************************************************************
 * *                                                                           *
 * *                          MACROS DEFINITIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */


/*
 * *****************************************************************************
 * *                                                                           *
 * *                           TYPE DEFINITIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

/*
 * *****************************************************************************
 * *                                                                           *
 * *                     STATIC FUNCTION DECLARATIONS                          *
 * *                                                                           *
 * *****************************************************************************
 */

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL VARIABLES                                *
 * *                                                                           *
 * *****************************************************************************
 */
RTC_DATA_ATTR static char s_current_image_name[128LU];

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL FUNCTIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

void RTC_IRAM_ATTR esp_wake_deep_sleep(void)
{
    esp_default_wake_deep_sleep();
}

const char* get_current_image_name(void)
{
    return (const char*)s_current_image_name;
}

void set_current_image_name(char* new_name, uint32_t new_name_length)
{
    int32_t bytes_written = snprintf(
                                s_current_image_name,
                                sizeof(s_current_image_name),
                                "%s",
                                new_name
                            );
    if (bytes_written < 0L) {
        LOG_ERROR("NO DATA WRITTEN INTO CURRENT IMAGE FILE NAME BUFFER");
        return;
    }

    if (bytes_written >= sizeof(s_current_image_name)) {
        LOG_ERROR("CURRENT IMAGE FILE NAME BUFFER TOO SMALL FOR FILE NAME");
        return;
    }

    /* Just to be sure */
    s_current_image_name[sizeof(s_current_image_name) - 1] = '\0';
}

/*
 * *****************************************************************************
 * *                                                                           *
 * *                            STATIC FUNCTIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */
