/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    http_client.h
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/
#ifndef COMPONENTS_IMAGE_HTTP_DOWNLOADER_INCLUDE_IMAGE_HTTP_DOWNLOADER_H_
#define COMPONENTS_IMAGE_HTTP_DOWNLOADER_INCLUDE_IMAGE_HTTP_DOWNLOADER_H_

#include <stdint-gcc.h>

typedef enum
{
    E_HTTP_OK,
    E_HTTP_INVALID_ARGS,
    E_HTTP_TIMEOUT,
    E_HTTP_INVALID_RESPONSE_CODE,
    E_HTTP_INTERNAL_ERROR,
} http_retval_e;

http_retval_e image_http_downloader_initialize(void);

http_retval_e image_http_downloader_request_next_image(
    const char* current_image_name,
    uint32_t    current_image_name_length
);

uint8_t* image_http_downloader_get_image_data(void);

int32_t image_http_downloader_get_image_size(void);

#endif /* COMPONENTS_IMAGE_HTTP_DOWNLOADER_INCLUDE_IMAGE_HTTP_DOWNLOADER_H_ */
