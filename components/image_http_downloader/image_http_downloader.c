/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    http_client.c
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 * 
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *  
 * ============================================================================= 
 **/

#include "image_http_downloader.h"
#include "rtc_wake_stub_current_image_name.h"

#include "logger.h"

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

#include "esp_system.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_http_client.h"
#include "esp_log.h"

/*
 * *****************************************************************************
 * *                                                                           *
 * *                          MACROS DEFINITIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */
#define IMAGE_RESPONSE_SIZE                                 (96512LU)
#define IMAGE_DOWNLOADER_SERVER_URL         "http://192.168.0.110:9192/download"

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           TYPE DEFINITIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

/*
 * *****************************************************************************
 * *                                                                           *
 * *                     STATIC FUNCTION DECLARATIONS                          *
 * *                                                                           *
 * *****************************************************************************
 */
static esp_err_t s_http_event_handle(esp_http_client_event_t *evt);

/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL VARIABLES                                *
 * *                                                                           *
 * *****************************************************************************
 */
static char s_image_response[IMAGE_RESPONSE_SIZE];

static int32_t s_image_size = 0LU;
/*
 * *****************************************************************************
 * *                                                                           *
 * *                           GLOBAL FUNCTIONS                                *
 * *                                                                           *
 * *****************************************************************************
 */

http_retval_e image_http_downloader_initialize(void)
{
    http_retval_e retval = E_HTTP_OK;


    return retval;
}

http_retval_e image_http_downloader_request_next_image(
    const char* current_image_name,
    uint32_t    current_image_name_length
)
{
    http_retval_e retval = E_HTTP_OK;
    esp_err_t err = ESP_OK;

    if (NULL == current_image_name) {
        LOG_ERROR("NULL CURRENT IMAGE NAME PROVIDED");
        retval = E_HTTP_INVALID_ARGS;
        goto exit;
    }

    char buffer[128LU];
    snprintf(
        buffer,
        sizeof(buffer),
        "{\n    \"current_filename\":\"%s\"\n}",
        current_image_name
    );

    esp_http_client_config_t config;
    memset(&config, 0LU, sizeof(config));

    config.url = IMAGE_DOWNLOADER_SERVER_URL;
    config.event_handler = s_http_event_handle;
    config.transport_type = HTTP_TRANSPORT_OVER_TCP;
    config.timeout_ms = 5000LU;
    config.buffer_size = IMAGE_RESPONSE_SIZE;
    config.user_data = s_image_response;

    esp_http_client_handle_t client = esp_http_client_init(&config);
    if (0LU == client) {
        LOG_ERROR("FAILED TO INITIALIZE HTTP CLIENT");
        retval = E_HTTP_INTERNAL_ERROR;
        goto exit;
    }

    esp_http_client_set_method(client, HTTP_METHOD_POST);
    esp_http_client_set_header(client, "Content-Type", "application/json");
    esp_http_client_set_post_field(client, buffer, (uint32_t)strlen(buffer));

    err = esp_http_client_perform(client);
    if (ESP_OK != err) {
        LOG_ERROR("FAILED TO SEND IMAGE HTTP POST REQUEST");
        retval = E_HTTP_INTERNAL_ERROR;
        goto cleanup;
    }

    uint32_t timeout_ms = 5000LU;

    while ((false == esp_http_client_is_complete_data_received(client)) &&
              (0LU != timeout_ms))
    {
        vTaskDelay(pdMS_TO_TICKS(100LU));
        timeout_ms -= 100LU;
    }

    if (0LU == timeout_ms) {
        LOG_ERROR("TIMEOUT! FAILED TO READ RESPONSE IN TIME");
    }

    int http_response_code = esp_http_client_get_status_code(client);
    LOG_INFO("HTTP RESPONSE CODE RECEIVED: %ld", http_response_code);
    int content_length = esp_http_client_get_content_length(client);
    LOG_INFO("HTTP RESPONSE CONTENT-LENGTH: %ld", content_length);

    if (200L != http_response_code) {
        LOG_ERROR("INVALID HTTP RESPONSE CODE RECEIVED: %ld",
                http_response_code);
        retval = E_HTTP_INVALID_RESPONSE_CODE;
    }
cleanup:
    esp_http_client_cleanup(client);
exit:
    return retval;
}

uint8_t* image_http_downloader_get_image_data(void)
{
    return (uint8_t*)s_image_response;
}

int32_t image_http_downloader_get_image_size(void)
{
    return s_image_size;
}



/*
 * *****************************************************************************
 * *                                                                           *
 * *                            STATIC FUNCTIONS                               *
 * *                                                                           *
 * *****************************************************************************
 */

static esp_err_t s_http_event_handle(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR: {
            LOG_ERROR("HTTP CLIENT EVENT ERROR");
        } break;

        case HTTP_EVENT_ON_CONNECTED: {
            LOG_INFO("HTTP CLIENT EVENT ON CONNECTED");
        } break;

        case HTTP_EVENT_HEADER_SENT: {
            LOG_INFO("HTTP CLIENT EVENT HEADER SENT");
        } break;

        case HTTP_EVENT_ON_HEADER: {
            LOG_INFO("HTTP_EVENT_ON_HEADER key=%s, value=%s",
                evt->header_key,
                evt->header_value);
            printf("%.*s", evt->data_len, (char*)evt->data);

            /* Check if the wnated header */
            if (NULL != strstr(evt->header_key, "Content-Disposition")) {
                char* filename_start = strstr(evt->header_value, "filename=");
                if (NULL != filename_start) {
                    filename_start += strlen("filename=");
                    uint32_t filename_length = strlen(filename_start);
                    set_current_image_name(filename_start, filename_length);
                }
            }
        } break;

        case HTTP_EVENT_ON_DATA: {
            if (!esp_http_client_is_chunked_response(evt->client)) {
                if (evt->user_data) {
                    memcpy(
                        &evt->user_data[s_image_size],
                        evt->data,
                        evt->data_len);
                    s_image_size += evt->data_len;
                }
                else {
                    memcpy(
                        &s_image_response[s_image_size],
                        evt->data,
                        evt->data_len);
                    s_image_size += evt->data_len;
                }
            }

        } break;

        case HTTP_EVENT_ON_FINISH: {
            LOG_INFO("HTTP CLIENT EVENT ON FINISH");
        } break;

        case HTTP_EVENT_DISCONNECTED: {
            LOG_INFO("HTTP CLIENT EVENT ON DISCONNECTED");
        } break;
    }

    return ESP_OK;
}
