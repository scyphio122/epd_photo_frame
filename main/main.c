/**
 * =============================================================================
 *
 * =============================================================================
 *
 *  @copyright MIT License
 *
 *  Copyright (c) 2020 Konrad Traczyk
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 *
 * =============================================================================
 *
 *  @file    main.c
 *
 *  @brief   << HERE GOES BRIEF DESCRIPTION OF FILE >>
 *
 *  @date  : Oct 28, 2020
 *  @author: Konrad Traczyk
 *
 *  @details << HERE GOES DETAILED DESCRIPTION OF FILE >>
 *
 *  @version 1.0 Initial version (Oct 28, 2020, KTR)
 *
 * =============================================================================
 *
 *  @todo
 *
 *
 * =============================================================================
 **/

#include <stdio.h>
#include <sys/time.h>

#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_wifi.h"
#include "esp_system.h"
#include "esp_sleep.h"
#include "esp_task_wdt.h"
#include "soc/rtc_wdt.h"

#include "default_image.h"
#include "epd.h"
#include "logger.h"
#include "rtc_wake_stub_current_image_name.h"
#include "image_http_downloader.h"
#include "wifi.h"


void app_main(void)
{
    const uint32_t TIME_TO_SLEEP_S = 60LU;
    const uint32_t uS_TO_S_FACTOR = 1000000LU;
    /* Pointer to the image which is to be displayed */
    uint8_t* image_buffer = NULL;
    /* Size of the image which is to be displayed */
    int32_t image_buffer_size = 0LU;

    log_init();

    /* Variables needed to calculate the delta time to be waited between device
     * awakes
     */
    uint32_t start_time_s = 0LU;
    uint32_t wakeup_time_s = 0LU;
    struct timeval tv_now;

    /* Get current time */
    gettimeofday(&tv_now, NULL);
    start_time_s = tv_now.tv_sec;

    /* Connect to WiFI network */
    if (0L != wifi_initialize()) {
        LOG_ERROR("FAILED TO CONNECT TO WIFI");
        /* In case of wifi error - default image will be displayed */
        image_buffer = (uint8_t*)default_image_get();
        image_buffer_size = default_image_get_size();
    }
    else {
        /* Initialize the HTTP image downloader */
        image_http_downloader_initialize();

        /* Get current displayed image name from the power-sustained RAM */
        const char* current_image_name = get_current_image_name();
        LOG_INFO("REQUESTING IMAGE: %s", current_image_name);

        /* Download the next image */
        http_retval_e http_retval = E_HTTP_OK;
        http_retval = image_http_downloader_request_next_image(
            current_image_name,
            strlen(current_image_name));

        /* If image download failed - set the default image to be displayed */
        if (E_HTTP_OK != http_retval) {
            LOG_ERROR("FAILED TO DONWLOAD FILE. WILL DISPLAY THE DEFAULT IMAGE");
            image_buffer = (uint8_t*)default_image_get();
            image_buffer_size = default_image_get_size();
        }
        else {
            /* If image download succeeded -
             * the downloaded image will be displayed
             */
            image_buffer = image_http_downloader_get_image_data();
            image_buffer_size = image_http_downloader_get_image_size();
        }
    }

    LOG_INFO("DISPLAYING IMAGE: %s", get_current_image_name());

    vTaskDelay(pdMS_TO_TICKS(2000LU));
    /* Initialize EPD display - instance 0 */
    epd_display_initialize(0LU);
    vTaskDelay(pdMS_TO_TICKS(2000LU));
    /* Send image data to the display */
    epd_display_write_image(0LU, image_buffer, image_buffer_size);
    vTaskDelay(pdMS_TO_TICKS(2000LU));
    /* Refresh the display */
    epd_display_refresh(0LU);
    /* Put display to deep sleep */
    epd_deep_sleep(0LU);

    /* Get current time */
    gettimeofday(&tv_now, NULL);
    /* Calculate how much time will need to be waited */
    wakeup_time_s = TIME_TO_SLEEP_S - (tv_now.tv_sec - start_time_s);
    LOG_INFO("WILL WAKEUP IN %u SECONDS", wakeup_time_s);

    /* Schedule the wakeup timer */
    esp_sleep_enable_timer_wakeup(wakeup_time_s * uS_TO_S_FACTOR);
    /* Go to deep sleep - will wake up by reset so no loop necessaty */
    esp_deep_sleep_start();
}
