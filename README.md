# EPD Display Photo Frame

This project aimed to develop an embedded device acting as 'electronic photo frame' with ability to automatically change the current picture. 

The device uses an E-Paper screen to display image in order to consume as little energy as possible in standby and provide nice, vintage look.

The heart of the device is based on the ESP32 board (ESP32 WRoom devkit v1 used for development). Thanks to this choice, the device has a possibility to download the next image from the local python server located in: (https://gitlab.com/scyphio122/epd_photo_frame_server).

## Description

The device consists of following components:
* WiFi driver
* Image downloader
* EPD display driver

For development purpose, the following setup was used:
* Display: https://www.good-display.com/product/244.html
* Waveshare EPD HAT: https://www.waveshare.com/wiki/E-Paper_Driver_HAT
* ESP32 devkit-v1: https://www.espressif.com/en/products/devkits/esp32-devkitc/overview

### WiFi driver
The WiFi component provides access to the network services.
To change the WiFi configuration (SSID, password and authentication method, change settings in the ```components/wifi/include/wifi_config.h```. Please create it based on the ```components/wifi/include/wifi_config.h.example``` file.

### Image downloader
This component is responsible for download of the next image to be displayed.
The only configurable settings are:
* Image display buffer: please change IMAGE_RESPONSE_SIZE macro in the image_http_downloader.c file (currently used 96512: (800 px * 480 px) / (8 bits / 2 bpp)
* Server URL: please change IMAGE_DOWNLOADER_SERVER_URL macro in the image_http_downloader.c file

### EPD display driver

The device uses the 7.5" Grayscale, 800x480px E-Paper Display from GoodDisplay (GDEW075T7) communicating over SPI. The display is able to display image in 4 Grayscale (2 Bits Per Pixel) tones.
It is assumed (not checked), that the project shall run with no problems on different EPD display this EPD display controller: UC8179. Pinout and resoulution settings shall be reconfigured

For communication with the EPD display, VSPI peripheral instance is being used with the following pinout:
* Reset pin: 15,
* Command/Data pin: 4,
* Busy pin: 22,
* SPI MOSI pin: 23,
* SPI MISO pin: not used,
* SPI SCK pin: 18,
* SPI CS pin: 5,

The SPI Frequency used: 10MHz.
The EPD display driver was developed in such a way that it shopuld be quite easy to port it to different RTOS and/or host device. All HW dependent functions are defined in epd_port.c file.

## License

The software is distributed based on the MIT License.